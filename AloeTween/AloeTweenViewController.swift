//
//  AloeTweenViewController.swift
//  AloeTween
//
//  Created by kawase yu on 2014/09/16.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

class AloeTweenViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate{

    private let currentFrame:CGRect = CGRectMake(0, 20, 50, 50)
    private let rect:UIView = UIView(frame: CGRectMake(0, 20, 50, 50))
    private var currentTween:AloeTween.AloeTweenObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pickerView:UIPickerView = UIPickerView()
        var pickerViewFrame:CGRect = pickerView.frame
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerViewFrame.origin.y = UIScreen.mainScreen().bounds.size.height - pickerViewFrame.size.height
        pickerView.frame = pickerViewFrame
        self.view.addSubview(pickerView)
        
        rect.backgroundColor = UIColor.redColor()
        self.view.addSubview(rect)
//        AloeTweenChain.create().add(2.0, ease: AloeEase.InBack, progress:{ (val) -> () in
//            let toX:CGFloat = 100.0 * val
//            let toY:CGFloat = 100.0 * val
//            self.rect.transform = CGAffineTransformMakeTranslation(toX, toY)
//        }).add(2.0, ease: AloeEase.InCirc, progress:{ (val) -> () in
//            let toX:CGFloat = 100.0 - (100.0 * val)
//            let toY:CGFloat = 100.0 + (50.0 * val)
//            self.rect.transform = CGAffineTransformMakeTranslation(toX, toY)
//        }).wait(4.0).call({ () -> () in
//            println("onComplete")
//        }).execute()
    }
    
    // MARK: UIPickerViewDelegate, UIPickerViewDataSource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return AloeEase.list.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        return AloeEase.list[row].name()
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        let ease:AloeEase = AloeEase.list[row]
        println("doTween:\(ease.name())")
        
        // リセット
        if(currentTween != nil){
            AloeTween.cancel(currentTween!)
        }
        rect.transform = CGAffineTransformMakeTranslation(0, 0)
        rect.frame = currentFrame
        
        // GO
        let duration:Double = 1.0
        weak var SELF:AloeTweenViewController? = self // この辺がわからん
        currentTween = AloeTween.doTween(duration, ease: ease, progress: { (val) -> () in
            let x:CGFloat = val * 270.0
            let y:CGFloat = val * 270.0
            SELF?.rect.transform = CGAffineTransformMakeTranslation(x, y)
        })
                
    }

}
